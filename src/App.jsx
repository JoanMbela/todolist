
function App() {
    return (
        <div className="App container bg-gray-200 min-h-screen">
            <input type="text" className="shadow-lg mt-4 ml-4 px-4 rounded h-10 min-w-[400px]" placeholder="To Do..."/>
            <button className="m-4 px-4 h-10 rounded shadow-md hover:shadow-lg duration-150 bg-slate-50">
                ajouter
            </button>
        </div>
    )
}

export default App